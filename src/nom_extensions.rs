macro_rules! try_parse_mut {
    ($i:expr, $($args:tt)*) => ({
        let (i, res) = try_parse!($i, $($args)* );
        $i = i;
        res
    });
}

/// Counterpart to bits,
/// `bytes!( parser ) => ( (&[u8], usize), &[u8] -> IResult<&[u8], T> ) -> IResult<(&[u8], usize), T>`,
/// transforms its bits stream input into a byte slice for the underlying parsers. If we start in the
/// middle of a byte throws away the bits until the end of the byte.
///
/// ```
/// # #[macro_use] extern crate nom;
/// # use nom::IResult::Done;
/// # use nom::rest;
/// # fn main() {
///  named!( parse<(u8, u8, &[u8])>,  bits!( tuple!(
///    take_bits!(u8, 4),
///    take_bits!(u8, 8),
///    bytes!(rest)
/// )));
///
///  let input = &[0xde, 0xad, 0xbe, 0xaf];
///
///  assert_eq!(parse( input ), Done(&[][..], (0xd, 0xea, &[0xbe, 0xaf][..])));
/// # }
macro_rules! bytes (
  ($i:expr, $submac:ident!( $($args:tt)* )) => (
    bytes_impl!($i, $submac!($($args)*));
  );
  ($i:expr, $f:expr) => (
    bytes_impl!($i, call!($f));
  );
);

#[doc(hidden)]
macro_rules! bytes_impl (
  ($macro_i:expr, $submac:ident!( $($args:tt)* )) => (
    {
      let inp;
      if $macro_i.1 % 8 != 0 {
        inp = & $macro_i.0[1 + $macro_i.1 / 8 ..];
      }
      else {
        inp = & $macro_i.0[$macro_i.1 / 8 ..];
      }

      match $submac!(inp, $($args)*) {
        $crate::IResult::Error(e) => {
          $crate::IResult::Error(e)
        }
        $crate::IResult::Incomplete(::nom::Needed::Unknown) => ::nom::IResult::Incomplete(::nom::Needed::Unknown),
        $crate::IResult::Incomplete(::nom::Needed::Size(i)) => {
          $crate::IResult::Incomplete(::nom::Needed::Size(i * 8))
        },
        $crate::IResult::Done(i, o) => {
          $crate::IResult::Done((i, 0), o)
        }
      }
    }
  );
);


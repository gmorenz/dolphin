use nom::IResult;

use std::convert::AsMut;

fn to_array<A, T>(slice: &[T]) -> A
where
    A: Sized + Default + AsMut<[T]>,
    T: Clone,
{
    let mut a = Default::default();
    <A as AsMut<[T]>>::as_mut(&mut a).clone_from_slice(slice);
    a
}

#[derive(Debug)]
pub struct MacAddress([u8; 6]);

impl MacAddress {
    fn parse(i: &[u8]) -> IResult<&[u8], MacAddress> {
        take!(i, 6).map(|x| MacAddress(to_array(x)))
    }
}

#[derive(Debug)]
pub struct Packet<'a> {
    pub mac_dest: MacAddress,
    pub mac_src: MacAddress,
    // TODO: Handle this properly with length/ether type union, QinQ, constants, etc.
    pub ether_type: u16,
    pub payload: &'a [u8],
    // TODO: Is there any way to determine if the packet includes the checksum? It seems
    // like the ones captured with pcap don't so we will assume it doesn't exist for now.
}

impl<'a> Packet<'a> {
    pub fn parse(i: &[u8]) -> IResult<&[u8], Packet> {
        // Can't use paths in do_parse
        let mac_parse = MacAddress::parse;
        use nom::{be_u16, rest};

        do_parse! { i,
               mac_dest: mac_parse
            >> mac_src: mac_parse
            >> ether_type: be_u16
            >> payload: rest
            >> (
                Packet {
                     mac_dest,
                     mac_src,
                     ether_type,
                     payload,
                 }
            )
        }
    }
}

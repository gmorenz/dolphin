use nom::{be_u8, be_u16, be_u32, IResult};

fn bit_flag(i: (&[u8], usize)) -> IResult<(&[u8], usize), bool> {
    map!(i, take_bits!(u8, 1), |x| x == 1)
}

/// Parsed name, allocates so we only have to deal with compression once
/// allowing easy comparison. There is an argument for making this just a
/// &packet_data, &start_of_name and parsing when used.
#[derive(PartialEq, Eq, Hash, Clone)]
pub struct Name(Vec<u8>);

#[derive(Debug)]
pub struct Question {
    pub name: Name,
    pub type_: u16,
    pub class: u16
}

#[derive(Debug)]
pub struct ResourceRecord<'a> {
    pub name: Name,
    pub type_: u16,
    pub class: u16,
    pub ttl: u32,
    pub data: &'a [u8]
}

#[derive(Debug)]
pub struct Packet<'a> {
    pub identifier: u16,
    pub response: bool,
    pub opcode: u8,
    pub authorative: bool,
    pub truncated: bool,
    pub recursion_desired: bool,
    pub recursion_available: bool,
    pub response_code: u8,
    pub questions: Vec<Question>,
    pub answers: Vec<ResourceRecord<'a>>,
    pub nameserver_count: u16,
    pub additional_record_count: u16,
}

impl Name {
    fn parser<'a>(packet_data: &'a [u8]) -> impl Fn(&'a [u8]) -> IResult<&'a [u8], Name> {
        move |mut i: &'a [u8]| {
            let initial_offset = i.as_ptr() as usize - packet_data.as_ptr() as usize;

            assert_eq!(packet_data.len(), initial_offset + i.len(), "Input isn't a slice to the end of the packet");

            let mut jump_barrier = initial_offset;
            let mut remaining = i;
            let mut output = vec![];

            loop {
                let byte = try_parse_mut!(i, be_u8);
                if byte == 0 {
                    if i.len() < remaining.len() {
                        remaining = i;
                    }
                    return IResult::Done(remaining, Name(output));
                }
                else if byte >= 0xc0 {
                    let b2 = try_parse_mut!(i, be_u8);
                    let offset = (((0x3f & byte) as usize) << 8) | b2 as usize;
                    if offset >= jump_barrier {
                        // TODO: How do error codes work?
                        return IResult::Error(error_code!(::nom::ErrorKind::Custom(54)));
                    }

                    if i.len() < remaining.len() {
                        remaining = i;
                    }

                    jump_barrier = offset;
                    i = &packet_data[offset..];
                }
                else {
                    output.push(byte);
                    let label = try_parse_mut!(i, take!(byte));
                    output.extend(label);
                }
            }
        }
    }
}

impl Question {
    fn parser<'a>(packet_data: &'a [u8]) -> impl Fn(&'a [u8]) -> IResult<&'a [u8], Question> {
        move |i: &'a [u8]| {
            let name_parser = Name::parser(packet_data);
            do_parse!( i,
                name: name_parser >>
                type_: be_u16 >>
                class: be_u16 >>
                ( Question { name, type_, class } )
            )
        }
    }
}

impl <'a> ResourceRecord<'a> {
    fn parser(packet_data: &'a [u8]) -> impl Fn(&'a [u8]) -> IResult<&'a [u8], ResourceRecord<'a>> {
        move |i: &'a [u8]| {
            let name_parser = Name::parser(packet_data);
            do_parse!( i,
                name: name_parser >>
                type_: be_u16 >>
                class: be_u16 >>
                ttl: be_u32 >>
                data_len: be_u16 >>
                data: take!(data_len) >>
                ( ResourceRecord{ name, type_, class, ttl, data } )
            )
        }
    }
}

impl <'a> Packet <'a> {
    pub fn parse(i: &'a [u8]) -> IResult<&'a [u8], Packet<'a>> {
        let question_parser = Question::parser(i);
        let record_parser = ResourceRecord::parser(i);
        bits!( i, do_parse! (
            identifier: take_bits!(u16, 16) >>
            response: bit_flag >>
            opcode: take_bits!(u8, 4) >>
            authorative: bit_flag >>
            truncated: bit_flag >>
            recursion_desired: bit_flag >>
            recursion_available: bit_flag >>
            // These bits are reseved (0), and then something to do with authentication (originally reserved 0).
            // TODO: Find out exactly how they are specified and handle.
            take_bits!(u8, 3) >>
            response_code: take_bits!(u8, 4) >>
            question_count: take_bits!(usize, 16) >>
            answer_count: take_bits!(usize, 16) >>
            nameserver_count: take_bits!(u16, 16) >>
            additional_record_count: take_bits!(u16, 16) >>
            questions: count!(bytes!(question_parser), question_count) >>
            answers: count!(bytes!(record_parser), answer_count) >>
            ( Packet{ identifier, response, opcode, authorative, truncated, recursion_desired,
                      recursion_available, response_code, questions,
                      answers, nameserver_count, additional_record_count } )
        ))
    }
}

impl ::std::fmt::Debug for Name {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        // TODO: Make a string with escaped characters or something.
        write!(f, "{}", String::from_utf8_lossy(&self.0))
    }
}
use nom::IResult;

#[derive(Debug)]
pub struct Packet<'a> {
    pub src_port: u16,
    pub dst_port: u16,
    pub len: u16,
    pub checksum: u16,
    pub payload: &'a [u8]
}

impl <'a> Packet<'a> {
    pub fn parse(i: &[u8]) -> IResult<&[u8], Packet> {
        use nom::be_u16;

        do_parse! ( i,
               src_port: be_u16
            >> dst_port: be_u16
            >> len: be_u16
            >> checksum: be_u16
            >> payload: take!(len - 8)
            >> (
                Packet {
                    src_port,
                    dst_port,
                    len,
                    checksum,
                    payload
                }
            )
        )
    }
}
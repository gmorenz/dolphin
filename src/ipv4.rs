use std::net::Ipv4Addr;

use nom::IResult;

#[derive(Debug)]
pub struct Packet<'a> {
    // not recorded is a version field, 4 bits in length, which must contain 4.

    /// Number of words in header, in words (32 bits), in range 5-15, 4 bits
    pub header_len: u8,
    /// 6 bits, used for real time streaming somehow?
    pub dscp: u8,
    /// 2 bits, congestion control
    pub ecn: u8,
    /// 16 bits, total length including header, min 20
    pub total_length: u16,
    /// 8 bits, used for identifying group fragments
    pub identification: u16,
    /// 3 bits
    pub flags: u8,
    /// 13 bits, relative to begining of ip datagram
    pub fragment_offset: u16,
    /// 8 bits, number of hops (strictly speaking rounded up seconds) before being dropped
    pub ttl: u8,
    /// 8 bits, protocol magic number
    pub protocol: u8,
    /// 16 bits, checksum (the router should check this?)
    pub checksum: u16,
    /// ipv4 address, source
    pub source_address: Ipv4Addr,
    /// ipv4 address, destination
    pub destination_address: Ipv4Addr,
    /// Currently we don't parse these, some number of options 0 padded to a 32 bit boundary
    pub options: &'a [u8],
    pub payload: &'a [u8]
}

impl<'a> Packet<'a> {
    pub fn parse(inp: &[u8]) -> IResult<&[u8], Packet> {
        bits!(
            inp,
            do_parse! (
                tag_bits!(u8, 4, 0x4)
                >> header_len: take_bits!(u8, 4)
                >> dscp: take_bits!(u8, 6)
                >> ecn: take_bits!(u8, 2)
                >> total_length: take_bits!(u16, 16)
                >> identification: take_bits!(u16, 16)
                >> flags: take_bits!(u8, 3)
                >> fragment_offset: take_bits!(u16, 13)
                >> ttl: take_bits!(u8, 8)
                >> protocol: take_bits!(u8, 8)
                >> checksum: take_bits!(u16, 16)
                >> source_address: take_bits!(u32, 32)
                >> destination_address: take_bits!(u32, 32)
                >> options: bytes!(take!(header_len * 4 - 20))
                >> payload: bytes!(take!(total_length - 4 * header_len as u16))
                >> (
                    Packet {
                        header_len,
                        dscp,
                        ecn,
                        total_length,
                        identification,
                        flags,
                        fragment_offset,
                        ttl,
                        protocol,
                        checksum,
                        source_address: Ipv4Addr::from(source_address),
                        destination_address: Ipv4Addr::from(destination_address),
                        options,
                        payload
                    }
                )
            )
        )
    }
}
